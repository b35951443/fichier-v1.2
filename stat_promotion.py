'''
    dsl j'ai pas put calculler l'age moyenne
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes v1.2 : H/F de la promotion
'''
import argparse
import datetime as dt
from dateutil import relativedelta as rd
# Import des fonctons de l'application
import functions.application_functions as af


VERSION = "1.2"

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str)
parser.add_argument("-l", "--list", action='store_true', help="Afficher la liste des étudiants")

args = parser.parse_args()
file = args.path

if file == None :
    file="promotion_B3_B.csv"
today=dt.datetime.now()
students_info = []

# Boucle de lecture ligne à ligne
with open(file, 'r') as file:
    next(file)  # On saute la première ligne (probablement les en-têtes)
    for line in file:
        if line[0] == '#':
            continue
        fields = line.strip().split(";")
        students_info.append(fields)
        gender = fields[2]
        bdate = fields[4]
        bdate = dt.datetime.strptime(bdate, '%d/%m/%Y')
sep="="*80
print(sep)
if not args.list:
    sep = "=" * 80
    nbr_tot = len(students_info)
    nbrh = sum([1 for student in students_info if student[2].lower() == 'h'])
    nbrf = sum([1 for student in students_info if student[2].lower() == 'f'])
    nbro = sum([1 for student in students_info if student[2].lower() not in ['h', 'f']])
    print(sep)
    print(f"Nombre total d'élèves : {nbr_tot}")
    print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
    print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
    print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")
    print(sep)
if args.list:
    print("Liste des étudiants :")
    for student in students_info:
        print(f"{student[1]} - {student[0]} - {student[4]}")
    print(sep)
# print(sep)
# print(f"Nombre total d'élèves : {nbr_tot}")
# print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
# print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
# print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")
# print(sep)

    
if args.path is None:
    print(f"Version actuelle : {VERSION}")
    
print("fin du programme...")    